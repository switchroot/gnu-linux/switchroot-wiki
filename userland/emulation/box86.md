# Box86

## Introduction

Running Linux version of Steam using box86 using chroot and install Wine.

Parts of this guide are based on this article by rna from armbian forum https://forum.armbian.com/topic/16584-install-box86-on-arm64/

## Using our prebuilt chroot

If you decide to use our preuilt chroot you only need to follow the section **Setting up the Host**.

## Setting up manually

### 1. Host side

We need debootstrap to create the chroot and schroot to enter the chroot.
We need to install virgl-test-server to have GPU acceleration.
*Note: Compiling it form source can provide FPS boost, depending on package age in repo of your os.*

1. Open terminal, then type:

```
sudo apt install schroot debootstrap virgl-server
sudo mkdir -p /srv/chroot/debian-armhf
```

2. Then add a config file for debian-armhf: 

```
sudo nano /etc/schroot/chroot.d/debian-armhf.conf
```

Copy paste this code, and change <username> into computer username.

```
[debian-armhf]
description=Debian Armhf chroot
aliases=debian-armhf
type=directory
directory=/srv/chroot/debian-armhf
profile=desktop
personality=linux
preserve-environment=true
root-users=<username>
users=<username>
```

3. Then edit the nssdatabases 

```
sudo nano /etc/schroot/desktop/nssdatabases
```

copy paste this code 

```
# System databases to copy into the chroot from the host system.
#
# <database name>
#passwd
shadow
#group
gshadow
services
protocols
#networks
#hosts
#user
```

4. Edit the stateoverride, and change the first contrab to root 

```
sudo nano /srv/chroot/debian-armhf/var/lib/dpkg/statoverride
```

copy paste this code

```
root root 2755 /usr/bin/crontab
```

5. Modify chroot fstab to not mount home directory by editing file 

```
sudo nano /etc/schroot/desktop/fstab
```

add "#" before line

```
/home           /home           none    rw,bind         0       0
```

6. Before we go to chroot, you need to start Xephyr virgl_test_server. Use below command in separate terminal.

```
Xephyr :2 & virgl_test_server --use-glx
```

### 2. Chroot side

1. Create the chroot using debootstrap

```
sudo debootstrap --arch armhf --foreign bullseye /srv/chroot/debian-armhf http://deb.debian.org/debian
sudo chroot "/srv/chroot/debian-armhf" /debootstrap/debootstrap --second-stage
```

2. Now you should be able to schroot by typing 

```
sudo schroot -c debian-armhf
```

3. edit the bashrc. If you want to use Xephyr on other port than 2, change that number.

```
nano /root/.bashrc
```

add this code to the bottom of the line 

```
export LANGUAGE="C"
export LC_ALL="C"
export DISPLAY=:2
export LIBGL_ALWAYS_SOFTWARE=1
export GALLIUM_DRIVER=virpipe
```

4. let's restart chroot environment by exit and login again, install sudo, then add a username that is similar to your main system username and give that user sudo privileges

```
echo "<username> ALL=(ALL:ALL) ALL" >> /etc/sudoers
apt install sudo
adduser <username>
usermod -aG sudo <username>
su - <username>
```

then again add bashrc 

```
nano /home/<username>/.bashrc
```

add this code to the bottom of the line 

```
export LANGUAGE="C"
export LC_ALL="C"
export DISPLAY=:2
export LIBGL_ALWAYS_SOFTWARE=1
export GALLIUM_DRIVER=virpipe
```

5. Then restart chroot by double exit, login again, add deb-src source to apt and buster source to have libappindicator1 in sources and then install the following packages 

```
exit
exit
schroot -c debian-armhf
sudo bash -c 'echo "deb-src http://deb.debian.org/debian bullseye main" >> /etc/apt/sources.list'
sudo bash -c 'echo "deb http://deb.debian.org/debian buster main" >> /etc/apt/sources.list'
sudo apt update && sudo apt upgrade
sudo apt install git wget cmake build-essential meson python3 gcc pciutils
sudo apt build-dep mesa
```

6. Now let's compile the Box86 within Chroot environment. Usage of RPI4 flags works fine on my Nano so Switch should also works fine. It have some additional workarounds.

```
git clone https://github.com/ptitSeb/box86
cd box86;mkdir build; cd build; cmake .. -DRPI4=1; make; sudo make install
```

7. It's time to compile and install Mesa driver. If you will have issues with main branch, try 20.3 or something after 21.2. At this moment vulkan don't work with Nvidia drivers.

```
git clone https://gitlab.freedesktop.org/mesa/mesa
cd mesa; meson build -Dgallium-drivers=virgl,swrast -Dvulkan-drivers=; cd build; ninja; sudo ninja install
```

8. Before we proceed further in chroot, we need to copy binfmt.d config from chroot to host and restart systemd-binfmt to make box86 works automaticly with bash scripts that includes i386 files.

```
exit
sudo cp /srv/chroot/debian-armhf/etc/binfmt.d/box86.conf /etc/binfmt.d/box86.conf
sudo systemctl restart systemd-binfmt
```

9. Time to install steam. Be sure that virgl-server is still running, then go back to main terminal and use command below.

```
schroot -c debian-armhf
sudo apt install libnm0 zenity nginx libgtk2.0-0 libdbus-glib-1-2 libxss1 libdbusmenu-gtk4 libsdl2-2.0-0 libice6 libsm6 libopenal1 libusb-1.0-0 libappindicator1
mkdir steam; cd steam; wget http://media.steampowered.com/client/installer/steam.deb; ar x steam.deb; tar xf data.tar.xz; STEAMOS=1 setarch -L linux32 ./usr/lib/steam/bin_steam.sh
```

You should be able to run steam in chroot with command ```STEAMOS=1 setarch -L linux32 ~/steam/usr/lib/steam/bin_steam.sh```

In Xephyr window you should see Steam. Browser won't work as It's 64-bit, in view choose small mode an this part should work. Keep in mind that It's unstable so it can crash.

About Wine, best option to install it is using steps form this guide: https://github.com/ptitSeb/box86/blob/master/docs/X86WINE.md

Adding i386 arch and installing package traditional way can cause issues with GPU acceleration in system.

Every time you start app from this chroot you need to have Xephyr and virgl_test_server running with command from earlier. You can define Xephyr resolution with -display variable.
