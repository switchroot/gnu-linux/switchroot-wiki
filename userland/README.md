# GNU/Linux distribution

Various linux distributions are avalaible to build.
You can use [Jet-Factory](https://github.com/switchroot/gnu-linux/Jet-Factory) to build a distribution for your Switch.

The state of each distributions avalaible is variable here is a little table reporting the state of it :

| Distro 	| State  | Version |
|-----------|--------|---------|
| Arch   	| Beta	 | 	  -	   |
| Bionic 	| Stable |  18.04  |
| Fedora 	| Beta   |   34    |
| Focal	 	| Stable |  20.04  |
| Gentoo	| Stable |    -    |
| Hirsute	| Stable |  21.04  |
| Tumbleweed| Alpha  |    -    |

## Distributions layout

```txt
bootloader/ini
     |_______L4T-XXXXXXX.ini

switchroot
  |_______install
	  |_______l4t.00        (4092 MiB parts. Last part can be 4 MiB aligned)
	  |_______l4t.01
	  ...
	  |_______l4t.XX
   |_______ubuntu
	   |_______boot.scr
	   |_______coreboot.rom
	   |_______Image
	   |_______initramfs
	   |_______tegra210-icosa.dtb
	   |_______uenv.txt
```

## Setting up your SD Card

1. Insert SD card to your PC

2. Download ![TegraRcmGUI](https://github.com/eliboa/TegraRcmGUI/releases/) for Windows and run "TegraRcmGUI_vX.X_installer.msi" to install TegraRCMGUI (follow on-screen instructions).

3. Launch "TegraRCMGUI" and Install The APX driver if needed.  This can be located in "Setting" -> "Install Driver"

4. Download ![Hekate](https://github.com/CTCaer/hekate/releases) latest release and extract "heakte_ctcaer_X.X.bin" 

5. Connect your switch to your PC/device via usb cable.

6. First, power off your Switch then insert the RCM Jig in the right joycon rail and press/hold VOLUME UP + POWER BUTTON for three seconds to enter into RCM

7. Find and select "heakte_ctcaer_X.X.bin" under "Select payload:" and hit "Inject payload". Then your Switch should now be in Hekate bootloader.

## Flashing a distribution for the first time

:First time Linux installation red:

* 1. Partition the sd card in order to create an ext4 partition.
hekate's partition manager can be used for that: Tools -> Partition SD Card.

The process in hekate is destructive, so you need to backup your fat partition files (if they are more than 1GB total) or emuMMC.

*Move the sliders* as you want them and then hit next. You will have plenty of chances/warns to backup your stuff if it's needed.

* 2. Make sure your Switch is connected via usb to your PC/Device and Select SD UMS
* 3. *Extract* the 2GB (standard) or the 4GB (developer cuda) 7z directly to SD
* 4. Safely unmount and flash via hekate's *Flash Linux*
* 5. Then go to Home and Nyx options and dump your joycon BT pairing
* 5. Return to Hekate's Home menu and choose "More Configs"
* 6. Boot

## Changing Reboot Options

To change the default reboot functionality, edit uenv.txt.
After `pmc_reboot2payload.reboot_action=`, write `via-payload` to have a reboot boot back into linux after shutdown or `bootloader` to reboot to Hekate.

Example: `pmc_reboot2payload.reboot_action=bootloader`

If you want to reboot to a specific payload from wihin the OS, execute `echo  "payload.bin" > /sys/devices/r2p/custom_payload` where `payload.bin` is located in the `/lib/firmware` directory

## Updating 

