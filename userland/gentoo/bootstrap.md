# Bootstrap Gentoo using QEMU

## Summary

Gentoo is a source based distribution which requires an existing Linux system for the initial system setup. Usually a live-medium is used, Gentoo-Live-DVD or any other Linux Live Disk, is used for this step.
The following instructions describes how to install Gentoo for Nintendo Switch using qemu emulator.

## Prerequisite

***Important Note:** This document expect that you use **Gentoo on your PC to build Gentoo for your Switch**.
Minimum 30GB of storage space avalaible on your PC.

## Host setup

### Installing QEMU

The official [Gentoo Howto](https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Compiling_with_qemu_user_chroot) (slightly outdated)

In a nutshell:

1. Check that `CONFIG_BINFMT_MISC` is set in your Kernel config.

2. QEMU needs to be compiled with `static-user` USE-flag and the Switch-compatible target `aarch64` :

```sh
echo 'app-emulation/qemu static-user' > /etc/portage/package.use/qemu
echo 'QEMU_USER_TARGETS="aarch64"' >> /etc/portage/make.conf
emerge app-emulation/qemu
```

3. The service `/etc/init.d/qemu-binfmt start` needs to be started in order for binfmt_misc to be able to register QEMU binaries for other architectures.

4. The trick is to copy `/usr/bin/qemu-aarch64` to an aarch64 root filesystem before execution. This way you can chroot into this root filesystem like you would with root filesystem of the same architecture. Extraction binary package is NOT necessary.

### Optional: Setup crossdev and distcc

To speed up compilation times I recommend that you use cross-compilation with distcc. Using this setup (crossdev+distcc) is advised to move compiling tasks away from compiler in emulation to native cross-compiler on host.

**Crossdev:**

The GCC version setup by crossdev should match against the one you'll require later to build Gentoo, we're using 10 for this example :

```sh
emerge crossdev
crossdev -S --gcc 10 -t aarch64-unknown-linux-gnu
```

**Distcc:**

Distcc shares CPU load between multiple devices within the same network 
```sh
emerge distcc
```

Then, add `--allow 127.0.0.0/24"` in `/etc/conf.d/distccd` to DISTCCD_OPTS.
`/etc/init.d/distccd` needs to be started.

### Optional: Enable cross-distcc for llvm/clang compiler

To enable cross-compiling in clang, set `USE=llvm_targets_AArch64` in make conf, or use the expand LLVM_TARGETS as you need. Recompile `sys-devel/clang` and `sys-devel/llvm` with the newly set USE flags.

Gentoo does not set up cross-distcc configuration for clang yet. See https://bugs.gentoo.org/651908. To enable this feature, some symlinks are required. Please use [bell07's fix-clang-distcc.sh](https://github.com/bell07/bashscripts-switch_gentoo/blob/master/tools/fix-clang-distcc.sh) to create this links.

## Pre-Chroot setup

Check the official installation instructions at https://wiki.gentoo.org/wiki/Handbook:PPC/Installation/Base, https://wiki.gentoo.org/wiki/Handbook:PPC/Installation/System and https://wiki.gentoo.org/wiki/Handbook:PPC/Installation/Finalizing (yea, PPC, there is no ARM documentation yet) for additional settings like timezone or locales.

### Installing Stage tarball

Dowload the latest [gentoo aarch64 stage 3](http://distfiles.gentoo.org/releases/arm64/autobuilds/current-stage3-arm64/)

Extract it to your build folder `/gentoo-switch`:
```sh
tar xvpf stage3-arm64-* --xattrs-include='*.*' --numeric-owner -C /gentoo-switch
```

### Configuring portage

#### Setup Switch-Gentoo Overlay

The Switch-Gentoo Repository contains ebuilds required for Gentoo on Switch.
**Therefore this step is not optional.**

```sh
git clone https://gitlab.com/bell07/gentoo-switch_overlay /gentoo-switch/var/db/repos/switch_overlay
mkdir /gentoo-switch/etc/portage/repos.conf
nano /gentoo-switch/etc/portage/repos.conf/switch_overlay.conf
```

The file editor should now be opened, paste the following content into it:

```sh
[switch]
location = /var/db/repos/switch_overlay
sync-type = git
sync-uri = https://gitlab.com/bell07/gentoo-switch_overlay
auto-sync = yes
```

#### Portage configuration

RTFM the [switch_overlay/README.md](https://gitlab.com/bell07/gentoo-switch_overlay/-/blob/master/README.md).

Remove all previous content from `etc/portage/make.conf` and setup the next parameters :

```
FEATURES="$FEATURES -pid-sandbox" # qemu caveat
FEATURES="$FEATURES distcc"       # Distribute compiling to the host crossdev

MAKEOPTS="-j6"                    # Parallel compiling tasks. Maybe other number is optimal
```

All other usual settings like CFLAGS are not necessary because they are set within the switch profile provided by the overlay.
See [make.defaults](https://gitlab.com/bell07/gentoo-switch_overlay/-/blob/master/profiles/nintendo_switch/make.defaults) in profile.

### Prepare

Copy qemu static binary to `switch-gentoo` to allow binfmt execution:
```sh
cp /usr/bin/qemu-aarch64 /gentoo-switch/usr/bin/
```

Copy `resolv.conf` for DNS resolution in chroot:
```sh
cp --dereference /etc/resolv.conf /gentoo-switch/etc/
```

Mount bind other special device needed for chroot :
```sh
mount --types proc /proc /gentoo-switch/proc
mount --rbind /sys /gentoo-switch/sys
mount --make-rslave /gentoo-switch/sys
mount --rbind /dev /gentoo-switch/dev
mount --make-rslave /gentoo-switch/dev
```

*Optional: you can bind-mount the portage and distfiles to your gentoo-host directories to avoid re-downloading.

## Bootstrap Gentoo

### Chroot and init portage

Chroot:
```sh
chroot /gentoo-switch /bin/bash
env-update
source /etc/profile
```

Synchronize/Fetch packages:
```sh
emerge-webrsync
```

List profiles avalaible:
```sh
eselect profile list
```

Select the switch profile avalaible that you wish to use. 
```sh
eselect profile set switch:nintendo_switch/17.0/desktop
```

Verify that the profile has been correctly set:
```sh
emerge --info
```

### Optional: setup distcc in chroot

If you followed the host-side distcc and crossdev configuration above, you have to setup distcc in your chroot in order to be able to share the workload.

Install distcc:
```sh
USE="-gtk -zeroconf" emerge -va distcc
```

Add your distcc host IP with port number (default port: 3632): `127.0.0.1:3632` to `/etc/distcc/hosts` to send compiling tasks to host distcc cross-compiler trough lo network interface instead of socket file.

### Update the toolchain

Update the toolchain packages to the latest version
*Note: The version should match versions used for cross-compiler:*

```sh
emerge --with-bdeps=n -1ua sys-devel/binutils sys-devel/gcc sys-kernel/linux-headers sys-libs/glibc
```

```sh
eselect binutils list
eselect binutils set updated_version
eselect gcc list
eselect gcc set updated_version
```

### Optional: Update @world

This is a good point to update or rebuild @world. You can also do this later, if gentoo is running nativelly on the switch.

### Install Switch base system files

Read again the [switch_overlay/README.md](https://gitlab.com/bell07/gentoo-switch_overlay/-/blob/master/README.md).
```sh
emerge -a --autounmask y nintendo-switch-meta
```

Accept the proposed keywords and licenses, let autounmask write the `/etc/portage/package.accept_keywords` and `/etc/portage/package.license` for you.

You should run `etc-update` or `dispatch-conf` to resolve conflict in your config files.

If the following message appears "*Multiple package instances within a single package slot have been pulled...*" (eg. because of bindist USE and because the world was not updated in previous step) This can be solved by rebuilding/updating affected packages:
```sh
emerge -a --oneshot dev-libs/openssl net-misc/openssh
```

Then try again:
```sh
emerge -a --autounmask y nintendo-switch-meta
```

### Compile and install the kernel

They are 3 ways to get working kernel
1. Recommended for quemu users is `sys-kernel/nintendo-switch-l4t-bin` that compiles and install kernel with default configuration.

2. To build on the switch the `sys-kernel/nintendo-switch-l4t-sources` can be used, as usual in gentoo in /usr/src
The default configuration is 

```sh
make tegra_linux_defconfig
```

Building as usual with`make`. Installation with

```sh
make modules_install
cp /usr/src/linux/arch/arm64/boot/dts/tegra210-icosa.dtb /boot
cp /usr/src/linux/arch/arm64/boot/Image /boot
```
3. Any other way like https://gitlab.com/switchroot/kernel/l4t-kernel-build-scripts


## Cleanup Gentoo after installation
If gentoo is up and running on the switch, and you plan to update your gentoo on the switch nativelly in feature, you need to cleanup the next settings:

 - remove `FEATURES=-pid-sandbox`. It's for qemu only
 - Disable also `FEATURES=distcc` or change the host in `/etc/distcc/hosts`
 - Remove the x86 binary /usr/bin/qemu-aarch64 copied into system above

## Install Gentoo-Switch

**Gentoo BOOT:**

```sh
mkdir /mnt/gentoo-switch-vfat
mount -t vfat /dev/sdX1 /mnt/gentoo-switch-vfat
cp -a /gentoo-switch/usr/share/sdcard1/* /mnt/gentoo-switch-vfat
```

**Gentoo ROOT:**

```sh
mkdir /mnt/gentoo-switch-ext4
mkfs.ext4 -L switch-gentoo /dev/sdX2
mount /dev/sdX2 /mnt/gentoo-switch-ext4
cp -a /gentoo-switch/* /mnt/gentoo-switch-ext4
```


# Next steps

If you miss some things or the system does not boot, you can mount the SD-Card and chroot it directly copying the qemu binary to SD-Card.
For debugging reasons enable the `hdmi_fbconsole=1` in your uenv.txt to see what appears.
